= Groogle Core
Jorge Aguilera <jorge.aguilera@puravida-software.com>
:description: Groogle Core repository

This is the Groogle Core repository.

This project belongs to the Groogle projects group. You can review them at https://gitlab.com/groogle[Groogle projects group]

Visit https://groogle.gitlab.io
