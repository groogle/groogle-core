package com.puravida.groogle.impl

import groovy.transform.CompileStatic

@CompileStatic
class GroogleCredentialsBuilder {

    GroogleCredentials credentials

}
