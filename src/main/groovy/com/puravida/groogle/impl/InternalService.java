package com.puravida.groogle.impl;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.auth.Credentials;
import com.puravida.groogle.Groogle;

public interface InternalService extends Groogle.GroogleService {
    void configure(JsonFactory jsonFactory,
                   HttpTransport httpTransport,
                   Credentials credentials,
                   String applicationName);
}
