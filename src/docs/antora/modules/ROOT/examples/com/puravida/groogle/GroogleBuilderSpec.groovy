package com.puravida.groogle

import com.google.auth.http.HttpCredentialsAdapter
import com.puravida.groogle.impl.GroovyGoogle
import spock.lang.Ignore
import spock.lang.Specification
import com.google.api.services.drive.Drive

class GroogleBuilderSpec extends Specification{

    void "simple login"(){

        when:
//tag::login[]
        Groogle groogle = GroogleBuilder.build {
            withOAuthCredentials{
                applicationName 'groogle-core'
                withScopes "https://www.googleapis.com/auth/drive"
                usingCredentials '/client_secret.json'
                storeCredentials false
            }
        }
//end::login[]
        and:
        groogle.login()

        then:
        groogle.credentials.logged
    }

    void "service login"(){

        when:
//tag::loginAsService[]
        Groogle groogle = GroogleBuilder.build {
            withServiceCredentials{
                withScopes "https://www.googleapis.com/auth/drive"
                usingCredentials '/service_secret.json'
            }
        }
//end::loginAsService[]
        and:
        groogle.login()

        then:
        groogle.credentials.logged
    }

    void "no store information"(){

        when:
//tag::loginNoStore[]
        Groogle groogle = GroogleBuilder.build {
            withOAuthCredentials{
                applicationName 'nostore'
                withScopes "https://www.googleapis.com/auth/drive"
                usingCredentials '/client_secret.json'
                storeCredentials false
            }
        }
//end::loginNoStore[]
        and:
        groogle.login()

        then:
        groogle.credentials.logged

        and:
        new File(System.getProperty("user.home"), ".credentials/nostore").exists()==false
    }

    @Ignore
    void "impersonate service login"(){
        when:
//tag::loginAsServiceImpersonate[]
        Groogle groogle = GroogleBuilder.build {
            withServiceCredentials{
                usingCredentials '/service_secret.json'
                withScopes "https://www.googleapis.com/auth/drive"
                accountUser 'groovy.groogle@gmail.com'
            }
        }
//end::loginAsServiceImpersonate[]
        and:
        groogle.login()

        then:
        groogle.credentials.logged

        when:
        def google = (GroovyGoogle)groogle
        def http = new HttpCredentialsAdapter(google.credentialsImpl.credentials)
        def drive = new Drive.Builder(
                google.credentialsImpl.httpTransport,
                google.credentialsImpl.jsonFactory,
                http)
                .build()
        then:
        drive.files().list().execute()

    }


    void "register service"(){

        when:
        Groogle groogle = GroogleBuilder.build {
            withOAuthCredentials{
                applicationName 'groogle-core'
                withScopes "https://www.googleapis.com/auth/drive"
                usingCredentials '/client_secret.json'
                storeCredentials true
            }
            register new DummyService(), DummyService
        }
        and:
        def service = groogle.service(DummyService)

        then:
        service.configured
    }
}
