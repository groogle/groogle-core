package com.puravida.groogle.test;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.auth.Credentials;
import com.puravida.groogle.Groogle;
import com.puravida.groogle.GroogleBuilder;
import com.puravida.groogle.impl.InternalService;
import org.junit.Test;

public class JavaBuilderTest {

    @Test
    public void buildConsumerInline(){
        try {
            //tag::build[]
            Groogle groogle = GroogleBuilder.build(it -> {
            });
            //end::build[]
        }catch(RuntimeException re){

        }
    }

    class TestService implements InternalService {
        @Override
        public void configure(JsonFactory jsonFactory, HttpTransport httpTransport, Credentials credentials, String applicationName) {

        }
    }

    @Test
    public void login(){
        //tag::login[]
        Groogle groogle = GroogleBuilder.build( dsl ->{ //<1>
            dsl
                    .withOAuthCredentials(withOAuth -> { // <2>
                        withOAuth
                                .applicationName("test")
                                .withScopes("https://www.googleapis.com/auth/drive")
                                .usingCredentials("/client_secret.json");
                    })
                    .register(new TestService(), TestService.class);
        });

        //end::login[]
        groogle.service(TestService.class);
    }

}
