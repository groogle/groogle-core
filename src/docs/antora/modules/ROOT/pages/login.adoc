= Login

First, it is necessary to log in to send a request to any API service. 

- OAuth: An user must provide their credentials (through the Google identification process) to allow the application the interaction in the name of the user on their account files.

- Service: The credentials are associated to a service account and it is not required an user identification. This kind of login only can access to the allowed resources.

As for `Groogle`, it allows to provide the requested identification data through a DSL with which this will be indicated: the application name, the path to credentails got from Google Console, etc.

== OAuth

=== Java

[source,java]
----
include::{examplesdir}/com/puravida/groogle/test/JavaBuilderTest.java[tags=login]
----
<1> A `Groogle` is built
<2> It is used a `Consumer<WithOAuthCredentials>` to provide the identification data

=== Groovy

[source,groovy]
----
include::{examplesdir}/com/puravida/groogle/GroogleBuilderSpec.groovy[tags=login]
----

=== Persist the credentials

With OAuth login, `Groogle` allows to persist the credentials in the user file system in order to not to ask for them to the user in the next application running.

In case of prefering the user to log in every time the application runs and not persisting the credentials, `Groogle` could be configured with `storeCredencials=false`

[source,groovy]
----
include::{examplesdir}/com/puravida/groogle/GroogleBuilderSpec.groovy[tags=loginNoStore]
----


== As Service

The Service login allows to use an account without user association.
Google will look for the `GOOGLE_APPLICATION_CREDENTIALS` environment variable in case not providing a Google credentials path. This environment variable is automatically provided when `Groogle` is running in any Cloud service as Compute Engine, Kubernetes Engine, etc

[source,groovy]
----
include::{examplesdir}/com/puravida/groogle/GroogleBuilderSpec.groovy[tags=loginAsService]
----

In the same way, some services allow *impersonalization* of an user (run the service in the name of their) as GMail does to send emails in the name of a certain user. It is possible to specify an `accountUser` or an `accountId` with the DSL.

[source,groovy]
----
include::{examplesdir}/com/puravida/groogle/GroogleBuilderSpec.groovy[tags=loginAsServiceImpersonate]
----

