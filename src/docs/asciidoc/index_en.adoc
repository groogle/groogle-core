= Groogle Core
:doctype: book
:toc: left
:toclevels: 4
:source-highlighter: coderay
:imagesdir: images
:icons: font
:setanchors:
:idprefix:
:idseparator: -
:toc-collapsable:
:google-analytics-code: UA-687332-11
:theme: colony
:ensure-https:

link:index.html[Version español]

include::english/intro.adoc[]
include::english/core.adoc[]
include::english/auth.adoc[]
include::english/drive.adoc[]
include::english/sheet.adoc[]
include::english/calendar.adoc[]
include::english/chart.adoc[]
include::english/maps.adoc[]

== Presentación

Slides link:./revealjs/[Presentación]


