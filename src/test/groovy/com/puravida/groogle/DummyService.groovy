package com.puravida.groogle

import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.auth.Credentials
import com.puravida.groogle.impl.InternalService


/**
 * @author : jorge <jorge.aguilera@seqera.io>
 *
 */
class DummyService implements InternalService{

    boolean configured = false

    @Override
    void configure(JsonFactory jsonFactory, HttpTransport httpTransport, Credentials credentials, String applicationName) {
        configured = true
    }
}
